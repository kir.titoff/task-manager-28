package ru.t1.ktitov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.command.AbstractCommand;
import ru.t1.ktitov.tm.dto.Domain;
import ru.t1.ktitov.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data/data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data/data.bin";

    @NotNull
    public static final String FILE_XML = "./data/data.xml";

    @NotNull
    public static final String FILE_JSON = "./data/data.json";

    @NotNull
    public static final String FILE_YAML = "./data/data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
