package ru.t1.ktitov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataJsonFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file by fasterxml";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON BY FASTERXML]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        if (!Files.exists(path.getParent()))
            Files.createDirectories(path.getParent());
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
