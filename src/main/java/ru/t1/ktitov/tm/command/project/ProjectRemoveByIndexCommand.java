package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove project by index";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
